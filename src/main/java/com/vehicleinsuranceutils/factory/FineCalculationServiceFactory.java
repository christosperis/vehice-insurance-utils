package com.vehicleinsuranceutils.factory;

import com.vehicleinsuranceutils.service.FineCalculationService;
import com.vehicleinsuranceutils.service.impl.FineCalculationDbService;
import com.vehicleinsuranceutils.service.impl.FineCalculationFileService;

public class FineCalculationServiceFactory {

    public static FineCalculationService getInstance(int dataSourceType) {
        FineCalculationService fineCalculationService;
        if (dataSourceType == 1) {
            fineCalculationService = new FineCalculationFileService();
        } else {
            fineCalculationService = new FineCalculationDbService();
        }
        return fineCalculationService;
    }
}
