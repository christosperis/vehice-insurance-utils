package com.vehicleinsuranceutils.repository;

import com.vehicleinsuranceutils.dao.VehicleDao;
import com.vehicleinsuranceutils.factory.DBConnectionFactory;
import com.vehicleinsuranceutils.mapper.DBVehicleMapper;
import com.vehicleinsuranceutils.model.Vehicle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class VehicleRepository implements VehicleDao {

    private final String GET_ALL_VEHICLES_QUERY = "SELECT A.PLATE_NUMBER, B.EXPIRY_DATE, C.OWNER_ID  " +
            "FROM VEHICLE A, INSURANCE B, OWNER C " +
            "WHERE A.INSURANCE_ID = B.INSURANCE_ID AND A.OWNER_ID = C.OWNER_ID " +
            "ORDER BY A.INSURANCE_ID;";

    private final String GET_VEHICLE_BY_PLATE_NUMBER_QUERY = "SELECT A.PLATE_NUMBER, B.EXPIRY_DATE, C.OWNER_ID  " +
            "FROM VEHICLE A, INSURANCE B, OWNER C " +
            "WHERE A.INSURANCE_ID = B.INSURANCE_ID AND A.OWNER_ID = C.OWNER_ID AND A.PLATE_NUMBER=?";

    private final String GET_FORECOMING_EXPIRIES_QUERY = "SELECT A.PLATE_NUMBER, B.EXPIRY_DATE, C.OWNER_ID  \n" +
            "FROM VEHICLE A, INSURANCE B, OWNER C \n" +
            "WHERE A.INSURANCE_ID = B.INSURANCE_ID AND A.OWNER_ID = C.OWNER_ID " +
            "AND str_to_date(B.EXPIRY_DATE,'%d/%c/%Y') > CURDATE() " +
            "AND str_to_date(B.EXPIRY_DATE,'%d/%c/%Y') < CURDATE() + INTERVAL ? DAY";

    @Override
    public Vehicle getVehicleByPlateNumber(String plateNumber) {
        Connection connection = DBConnectionFactory.getConnection();

        try {
            PreparedStatement stmt = connection.prepareStatement(GET_VEHICLE_BY_PLATE_NUMBER_QUERY);
            stmt.setString(1, plateNumber);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return DBVehicleMapper.mapVehicleToRs(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        Connection connection = DBConnectionFactory.getConnection();
        List<Vehicle> vehicles = new ArrayList<>();

        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(GET_ALL_VEHICLES_QUERY);
            while (rs.next()) {
                vehicles.add(DBVehicleMapper.mapVehicleToRs(rs));
            }
            return vehicles;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vehicles;
    }

    @Override
    public List<Vehicle> getForecomingExpiries(String dateRange) {
        Connection connection = DBConnectionFactory.getConnection();
        List<Vehicle> vehicles = new ArrayList<>();

        try {
            PreparedStatement stmt = connection.prepareStatement(GET_FORECOMING_EXPIRIES_QUERY);
            stmt.setInt(1, Integer.valueOf(dateRange));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                vehicles.add(DBVehicleMapper.mapVehicleToRs(rs));
            }
            return vehicles;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vehicles;

    }

}
