package com.vehicleinsuranceutils.service;

import com.vehicleinsuranceutils.model.Vehicle;
import com.vehicleinsuranceutils.utils.StringDateHelper;

import java.util.ArrayList;
import java.util.List;

public abstract class FineCalculationService {

    public abstract int checkOwnersFines(Integer ownerId, Integer fine);

    protected int calculateFine(List<Vehicle> ownersVehicles, Integer fine) {
        List<Vehicle> expiredVehicles = new ArrayList<>();
        for (Vehicle vehicle : ownersVehicles) {
            if (!StringDateHelper.isExpiredInsurance(vehicle.getInsuranceDate())) {
                expiredVehicles.add(vehicle);
            }
        }
        return expiredVehicles.size() * fine;
    }


}
