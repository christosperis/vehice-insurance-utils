package com.vehicleinsuranceutils.factory;

import com.vehicleinsuranceutils.service.InsuranceStatusService;
import com.vehicleinsuranceutils.service.impl.InsuranceStatusDbService;
import com.vehicleinsuranceutils.service.impl.InsuranceStatusFileService;

public class InsuranceStatusServiceFactory {

    public static InsuranceStatusService getInstance(int dataSourceType) {
        InsuranceStatusService insuranceStatusService;
        if (dataSourceType == 1) {
            insuranceStatusService = new InsuranceStatusFileService();
        } else {
            insuranceStatusService = new InsuranceStatusDbService();
        }
        return insuranceStatusService;
    }

}
