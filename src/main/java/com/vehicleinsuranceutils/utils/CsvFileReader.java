package com.vehicleinsuranceutils.utils;

import java.io.*;

public class CsvFileReader {

    private static String CSV_FILE = "VehiclesData.csv";
    public static int MAX_ARRAY_ROW_SIZE = 110;
    public static int MAX_ARRAY_COLUMN_SIZE = 5;

    public static String[][] readCsv() {

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        String[][] parsedVehiclesData = new String[MAX_ARRAY_ROW_SIZE][MAX_ARRAY_COLUMN_SIZE];

        try {

            br = new BufferedReader(new FileReader(getFileResource()));
            int rowCount = 0;
            while ((line = br.readLine()) != null) {

                // use comma as separator
                parsedVehiclesData[rowCount] = line.split(cvsSplitBy);
                rowCount++;
                //System.out.println("Plate [number= " + plates[0] + " , tax id=" + plates[1] + ", date=" + plates[2] +"]");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return parsedVehiclesData;
        }

    }

    private static File getFileResource() {
        ClassLoader classLoader = CsvFileReader.class.getClassLoader();
        return new File(classLoader.getResource(CSV_FILE).getFile());
    }


/*    public static void main(String[] args) {
        readCsv();
    }*/


}
