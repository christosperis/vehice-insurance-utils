package com.vehicleinsuranceutils.service.impl;

import com.vehicleinsuranceutils.mapper.FileToVehicleMapper;
import com.vehicleinsuranceutils.model.Vehicle;
import com.vehicleinsuranceutils.service.FineCalculationService;

import java.util.ArrayList;
import java.util.List;

public class FineCalculationFileService extends FineCalculationService {

    public int checkOwnersFines(Integer ownerId, Integer fine) {
        List<Vehicle> vehicleList = FileToVehicleMapper.getVehicleListFromFile();
        List<Vehicle> ownersVehicles = new ArrayList<>();
        for (Vehicle vehicle : vehicleList) {
            if (vehicle.getOwner().equals(ownerId)) {
                ownersVehicles.add(vehicle);
            }
        }
        return calculateFine(ownersVehicles, fine);

    }

}
