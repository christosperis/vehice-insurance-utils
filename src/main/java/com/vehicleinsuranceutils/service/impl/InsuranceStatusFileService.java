package com.vehicleinsuranceutils.service.impl;

import com.vehicleinsuranceutils.mapper.FileToVehicleMapper;
import com.vehicleinsuranceutils.model.Vehicle;
import com.vehicleinsuranceutils.service.InsuranceStatusService;
import com.vehicleinsuranceutils.utils.StringDateHelper;

import java.util.Map;

import static com.vehicleinsuranceutils.enumeration.MessageEnum.VEHICLE_PLATE_EXPIRATION;
import static com.vehicleinsuranceutils.service.impl.VehicleListingDbService.FILE_DELIMITER;


public class InsuranceStatusFileService extends InsuranceStatusService {

    public String fetchVehicleInsuranceData(String plateNumber) {

        Map<String, Vehicle> mappedVehicles = FileToVehicleMapper.getStringVehicleMap();
        return checkInsuranceStatus(mappedVehicles.get(plateNumber));

    }

    public String checkForecomingExpiries(String daysRange) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Vehicle> mappedVehicles = FileToVehicleMapper.getStringVehicleMap();

        for (Map.Entry<String, Vehicle> entry : mappedVehicles.entrySet()) {
            if (StringDateHelper.expiresInRange(entry.getValue().getInsuranceDate(), daysRange)) {
                stringBuilder.append(String.format(VEHICLE_PLATE_EXPIRATION.getMessage(), entry.getValue().exportCsvFormatted(FILE_DELIMITER.charAt(0))));
            }
        }
        return stringBuilder.toString();
    }

}
