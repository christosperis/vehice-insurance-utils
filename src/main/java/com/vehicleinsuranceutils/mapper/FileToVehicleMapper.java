package com.vehicleinsuranceutils.mapper;

import com.vehicleinsuranceutils.model.Vehicle;
import com.vehicleinsuranceutils.utils.CsvFileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileToVehicleMapper {

    public static Map<String, Vehicle> getStringVehicleMap() {
        String[][] mappedFile = CsvFileReader.readCsv();

        Map<String, Vehicle> mappedVehicles = new HashMap<>();

        for (int i = 0; mappedFile[i].length > 0; i++) {
            mappedVehicles.put(mappedFile[i][0], new Vehicle(mappedFile[i][0], mappedFile[i][2], Integer.valueOf(mappedFile[i][1])));
        }
        return mappedVehicles;
    }

    public static List<Vehicle> getVehicleListFromFile() {
        String[][] mappedFile = CsvFileReader.readCsv();

        List<Vehicle> mappedVehicles = new ArrayList<>();

        for (int i = 0; mappedFile[i].length > 0; i++) {
            mappedVehicles.add(new Vehicle(mappedFile[i][0], mappedFile[i][2], Integer.valueOf(mappedFile[i][1])));
        }
        return mappedVehicles;
    }

}
