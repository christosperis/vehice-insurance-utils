package com.vehicleinsuranceutils.service;

import com.vehicleinsuranceutils.exception.VehicleListingServiceException;
import com.vehicleinsuranceutils.model.Vehicle;

import java.util.List;

public abstract class VehicleListingService {

    public static String FILE_DELIMITER = ";";

    public abstract String listOrderedVehicles(String rowsNum) throws VehicleListingServiceException;

    protected void bubbleSortVehicles(List<Vehicle> vehicleList) {
        for (int i = vehicleList.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (vehicleList.get(j + 1) == null) {
                    continue;
                }
                if (vehicleList.get(j) == null || vehicleList.get(j + 1).compareTo(vehicleList.get(j)) < 0) {
                    Vehicle temp = vehicleList.get(j + 1);
                    vehicleList.set(j + 1, vehicleList.get(j));
                    vehicleList.set(j, temp);
                }
            }
        }
    }

    protected String prepareResponse(List<Vehicle> vehicles) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Vehicle vehicle : vehicles) {
            stringBuilder.append(vehicle.exportCsvFormatted(FILE_DELIMITER.charAt(0)));
        }
        return stringBuilder.toString();
    }
}
