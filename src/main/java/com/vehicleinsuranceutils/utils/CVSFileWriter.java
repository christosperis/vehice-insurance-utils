package com.vehicleinsuranceutils.utils;

import java.io.FileWriter;
import java.io.IOException;

public class CVSFileWriter {

    private static String CSV_FILE = "VehicleExport.csv";

    public static void writeToCsvFile (String response) {

        try {
            FileWriter writer = new FileWriter(CSV_FILE);
            writer.append(response);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
