package com.vehicleinsuranceutils.factory;

import com.vehicleinsuranceutils.utils.DatabaseConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnectionFactory {

    public static Connection getConnection() {
        try {
            DatabaseConfiguration databaseConfiguration = loadDatabaseConfiguration();
            return DriverManager.getConnection(
                    databaseConfiguration.getDatabaseUrl(),
                    databaseConfiguration.getUsername(),
                    databaseConfiguration.getPassword()
            );
        } catch (SQLException ex) {
            throw new RuntimeException("Error connecting to the database", ex);
        }

    }

    private static DatabaseConfiguration loadDatabaseConfiguration() {
        Properties properties = new Properties();

        try (InputStream input = DBConnectionFactory.class.getClassLoader().getResourceAsStream("application.properties")) {
            properties.load(input);

            return new DatabaseConfiguration(
                    properties.getProperty("database_user"),
                    properties.getProperty("database_password"),
                    properties.getProperty("database_url")
            );
        } catch (IOException ex) {
            throw new RuntimeException("Failed to connect to database");
        }
    }

}
