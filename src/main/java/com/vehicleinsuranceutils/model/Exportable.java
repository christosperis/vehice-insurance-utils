package com.vehicleinsuranceutils.model;

public interface Exportable{

    String exportCsvFormatted(char delimiter);

}
