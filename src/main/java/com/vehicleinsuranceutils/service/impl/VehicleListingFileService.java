package com.vehicleinsuranceutils.service.impl;

import com.vehicleinsuranceutils.exception.VehicleListingServiceException;
import com.vehicleinsuranceutils.mapper.FileToVehicleMapper;
import com.vehicleinsuranceutils.model.Vehicle;
import com.vehicleinsuranceutils.service.VehicleListingService;

import java.util.Collections;
import java.util.List;

public class VehicleListingFileService extends VehicleListingService {

    public String listOrderedVehicles(String rowsNum) throws VehicleListingServiceException {
        List<Vehicle> allVehicles = FileToVehicleMapper.getVehicleListFromFile();
        Collections.sort(allVehicles);
        try {
            return prepareResponse(allVehicles.subList(0, Integer.valueOf(rowsNum)));
        } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
            throw new VehicleListingServiceException("Number provided exceeds available vehicle number", e);
        }
    }

}
