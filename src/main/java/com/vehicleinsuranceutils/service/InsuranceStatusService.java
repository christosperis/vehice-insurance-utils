package com.vehicleinsuranceutils.service;

import com.vehicleinsuranceutils.model.Vehicle;
import com.vehicleinsuranceutils.utils.StringDateHelper;

import static com.vehicleinsuranceutils.enumeration.MessageEnum.INVALID_INSURANCE;
import static com.vehicleinsuranceutils.enumeration.MessageEnum.NO_RECORDS_FOUND;
import static com.vehicleinsuranceutils.enumeration.MessageEnum.VALID_INSURANCE;

public abstract class InsuranceStatusService {


    public abstract String fetchVehicleInsuranceData(String plateNumber);

    public abstract String checkForecomingExpiries(String daysRange);

    protected String checkInsuranceStatus(Vehicle vehicle) {

        if (vehicle != null && vehicle.getPlateNumber() != null) {
            String expiryDate = vehicle.getInsuranceDate();
            if (StringDateHelper.isExpiredInsurance(expiryDate)) {
                return String.format(VALID_INSURANCE.getMessage(), expiryDate);
            } else {
                return String.format(INVALID_INSURANCE.getMessage(), expiryDate);
            }
        } else {
            return NO_RECORDS_FOUND.getMessage();
        }

    }


}
