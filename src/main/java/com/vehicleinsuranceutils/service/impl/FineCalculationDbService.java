package com.vehicleinsuranceutils.service.impl;

import com.vehicleinsuranceutils.model.Vehicle;
import com.vehicleinsuranceutils.repository.OwnerRepository;
import com.vehicleinsuranceutils.service.FineCalculationService;

import java.util.List;

public class FineCalculationDbService extends FineCalculationService {

    public int checkOwnersFines(Integer ownerId, Integer fine) {
        OwnerRepository ownerRepository = new OwnerRepository();
        List<Vehicle> ownersVehicles = ownerRepository.fetchOwnerVehicles(ownerId);
        return calculateFine(ownersVehicles, fine);
    }

}
