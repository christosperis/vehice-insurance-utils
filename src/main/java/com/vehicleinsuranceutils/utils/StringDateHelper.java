package com.vehicleinsuranceutils.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class StringDateHelper {

    public static final String DATE_FORMAT = "d/M/yyyy";

    public static boolean isExpiredInsurance(String stringDate) {
        return LocalDate.parse(stringDate, DateTimeFormatter.ofPattern(DATE_FORMAT)).compareTo(LocalDate.now()) > 0;
    }

    public static boolean expiresInRange(String stringDate, String daysRange) {
        LocalDate vehiclesDate = LocalDate.parse(stringDate, DateTimeFormatter.ofPattern(StringDateHelper.DATE_FORMAT));
        return (vehiclesDate.compareTo(LocalDate.now()) > 0 &&
                vehiclesDate.compareTo(LocalDate.now().plusDays(Long.valueOf(daysRange))) < 0);
    }

}
