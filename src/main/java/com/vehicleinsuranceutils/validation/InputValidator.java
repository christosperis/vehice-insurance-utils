package com.vehicleinsuranceutils.validation;

import java.math.BigInteger;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class InputValidator {

    public static final String PLATE_NUMBER_FORMAT = "[ABEHIKMNOPTOXYZ]{3}-[0123456789]{4}";

    public static boolean validatePlateNumber(String plateNumber) {
        final Pattern pattern = Pattern.compile(PLATE_NUMBER_FORMAT);
        if (!pattern.matcher(plateNumber).matches()) {
            return false;
        }
        return true;
    }

    public static int fetchAndValidateInput(Scanner scanner, int lowLimit, int upperlimit) {
        int input = 0;
        boolean inputValidate = false;
        do {
            System.out.print("Please enter a valid option: ");
            try {
                input = scanner.nextInt();
                if (input >= lowLimit && input <= upperlimit) {
                    inputValidate = true;
                } else {
                    System.out.println(String.format("Not in range. Please input number %s-%s.", lowLimit, upperlimit));
                    scanner.nextLine();
                }
            } catch (InputMismatchException exception) {
                System.out.println(String.format("Not in range. Please input number %s-%s.", lowLimit, upperlimit));
                scanner.nextLine();
            }

        } while (!(inputValidate));
        return input;
    }


}
