package com.vehicleinsuranceutils.input;

import com.vehicleinsuranceutils.enumeration.InputEnum;
import com.vehicleinsuranceutils.exception.VehicleListingServiceException;
import com.vehicleinsuranceutils.factory.FineCalculationServiceFactory;
import com.vehicleinsuranceutils.factory.InsuranceStatusServiceFactory;
import com.vehicleinsuranceutils.factory.VehicleListingServiceFactory;
import com.vehicleinsuranceutils.service.FineCalculationService;
import com.vehicleinsuranceutils.service.InsuranceStatusService;
import com.vehicleinsuranceutils.service.VehicleListingService;
import com.vehicleinsuranceutils.utils.CVSFileWriter;
import com.vehicleinsuranceutils.validation.InputValidator;

import java.util.Scanner;

public class InsuranceApplication {

    private static final int MAX_FINE = 10000;
    private static final int MAX_OWNER_ID = 999999999;

    public static void main(String[] args) {
        // create a scanner so we can read the command-line input
        Scanner scanner = new Scanner(System.in);

        //  prompt for the user's request
        System.out.println("---Select Functionality to perform:");
        System.out.println("*1 Vehicle Insurance status");
        System.out.println("*2 Forecoming Expiries");
        System.out.println("*3 Vehicles ordered by plate");
        System.out.println("*4 Find Owner's owed Fine");

        int selectedUtility = InputValidator.fetchAndValidateInput(scanner, 1, 4);

        // prompt for preferred export
        System.out.println("---Enter data source: ");
        System.out.println("*1 File");
        System.out.println("*2 DB");
        int dataSourceType = InputValidator.fetchAndValidateInput(scanner, 1, 2);

        // prompt for preferred export
        System.out.println("---Enter export type: ");
        System.out.println("*1 File");
        System.out.println("*2 Console");

        int exportType = InputValidator.fetchAndValidateInput(scanner, 1, 2);

        String result = processInput(scanner, InputEnum.getInputEnum(selectedUtility), dataSourceType);
        exportResponse(exportType, result);

    }


    private static String processInput(Scanner scanner, InputEnum selectedUtility, int dataSourceType) {

        InsuranceStatusService insuranceStatusService;
        VehicleListingService vehicleListingService;
        FineCalculationService fineCalculationService;
        String response = "";

        switch (selectedUtility) {
            case VEHICLE_INSURANCE_STATUS:
                System.out.println("---Please provide plate number in ABC-1234 pattern: ");
                String plateNumber = scanner.next();
                while (!InputValidator.validatePlateNumber(plateNumber)) {
                    System.out.println(String.format("Provided plate %s does not match ABC-1234 format, please provide valid format plate: ", plateNumber));
                    plateNumber = scanner.next();
                }
                insuranceStatusService = InsuranceStatusServiceFactory.getInstance(dataSourceType);
                response = insuranceStatusService.fetchVehicleInsuranceData(plateNumber);
                break;
            case FORECOMING_EXPIRIES:
                System.out.println("---Please provide the range of days for the forecoming expiries");
                int daysRange = InputValidator.fetchAndValidateInput(scanner, 1, 365);
                insuranceStatusService = InsuranceStatusServiceFactory.getInstance(dataSourceType);
                response = insuranceStatusService.checkForecomingExpiries(String.valueOf(daysRange));
                break;
            case ORDER_VEHICLES:
                System.out.println("---Please provide the number of records you would like to be retrieved");
                String rowsNum = scanner.next();
                vehicleListingService = VehicleListingServiceFactory.getInstance(dataSourceType);
                try {
                    response = vehicleListingService.listOrderedVehicles(rowsNum);
                } catch (VehicleListingServiceException e) {
                    System.out.println(e.getMessage());
                }
                break;
            case OWNERS_FINE:
                System.out.println("---Please provide Owner's id");
                int ownerId = InputValidator.fetchAndValidateInput(scanner, 1, MAX_OWNER_ID);
                System.out.println("---Please provide Fine's value");
                int fine = InputValidator.fetchAndValidateInput(scanner, 1, MAX_FINE);
                fineCalculationService = FineCalculationServiceFactory.getInstance(dataSourceType);
                response = String.format("The Owner %s owes a total of %s", ownerId, String.valueOf(fineCalculationService.checkOwnersFines(Integer.valueOf(ownerId), fine)));
                break;
            default:
                response = "The provided functionality is not recognized";
        }

        return response;
    }

    private static void exportResponse(int exportType, String response) {
        if (exportType == 1) {
            CVSFileWriter.writeToCsvFile(response);
        } else {
            System.out.println(response);
        }
    }

}
