package com.vehicleinsuranceutils.service.impl;

import com.vehicleinsuranceutils.model.Vehicle;
import com.vehicleinsuranceutils.repository.VehicleRepository;
import com.vehicleinsuranceutils.service.InsuranceStatusService;
import com.vehicleinsuranceutils.utils.StringDateHelper;

import java.util.List;

import static com.vehicleinsuranceutils.enumeration.MessageEnum.VEHICLE_PLATE_EXPIRATION;
import static com.vehicleinsuranceutils.service.impl.VehicleListingDbService.FILE_DELIMITER;


public class InsuranceStatusDbService extends InsuranceStatusService {

    public String fetchVehicleInsuranceData(String plateNumber) {
        VehicleRepository vehicleRepository = new VehicleRepository();
        return checkInsuranceStatus(vehicleRepository.getVehicleByPlateNumber(plateNumber));

    }

    public String checkForecomingExpiries(String daysRange) {
        StringBuilder stringBuilder = new StringBuilder();
        VehicleRepository vehicleRepository = new VehicleRepository();
        List<Vehicle> vehicleList = vehicleRepository.getForecomingExpiries(daysRange);

        for (Vehicle vehicle : vehicleList) {
            if (StringDateHelper.expiresInRange(vehicle.getInsuranceDate(), daysRange)) {
                stringBuilder.append(String.format(VEHICLE_PLATE_EXPIRATION.getMessage(), vehicle.exportCsvFormatted(FILE_DELIMITER.charAt(0)))); //returns a CSV formatted string
            }
        }
        return stringBuilder.toString();
    }

}
