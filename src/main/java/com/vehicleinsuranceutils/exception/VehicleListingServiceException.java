package com.vehicleinsuranceutils.exception;

public class VehicleListingServiceException extends Exception {

    public VehicleListingServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
