package com.vehicleinsuranceutils.factory;

import com.vehicleinsuranceutils.service.VehicleListingService;
import com.vehicleinsuranceutils.service.impl.VehicleListingDbService;
import com.vehicleinsuranceutils.service.impl.VehicleListingFileService;

public class VehicleListingServiceFactory {

    public static VehicleListingService getInstance(int dataSourceType) {
        VehicleListingService vehicleListingService;
        if (dataSourceType == 1) {
            vehicleListingService = new VehicleListingFileService();
        } else {
            vehicleListingService = new VehicleListingDbService();
        }
        return vehicleListingService;
    }

}
