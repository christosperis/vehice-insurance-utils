package com.vehicleinsuranceutils.repository;

import com.vehicleinsuranceutils.dao.OwnerDao;
import com.vehicleinsuranceutils.factory.DBConnectionFactory;
import com.vehicleinsuranceutils.mapper.DBVehicleMapper;
import com.vehicleinsuranceutils.model.Vehicle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OwnerRepository implements OwnerDao {

    private final String GET_OWNER_AND_INSURANCE_BY_OWNER_ID_QUERY = "SELECT A.PLATE_NUMBER, B.EXPIRY_DATE, A.OWNER_ID FROM VEHICLE A, INSURANCE B WHERE A.INSURANCE_ID = B.INSURANCE_ID AND A.OWNER_ID=?";

    @Override
    public List<Vehicle> fetchOwnerVehicles(Integer ownerId) {
        Connection connection = DBConnectionFactory.getConnection();

        try {
            PreparedStatement stmt = connection.prepareStatement(GET_OWNER_AND_INSURANCE_BY_OWNER_ID_QUERY);
            stmt.setInt(1, ownerId);
            ResultSet rs = stmt.executeQuery();
            List<Vehicle> vehicles = new ArrayList<>();
            while (rs.next()) {
                Vehicle vehicle = DBVehicleMapper.mapVehicleToRs(rs);
                vehicles.add(vehicle);
            }
            return vehicles;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
