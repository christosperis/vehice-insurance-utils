package com.vehicleinsuranceutils.enumeration;

public enum InputEnum {

    VEHICLE_INSURANCE_STATUS(1),
    FORECOMING_EXPIRIES(2),
    ORDER_VEHICLES(3),
    OWNERS_FINE(4);

    private final int type;

    InputEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static InputEnum getInputEnum(Integer inputValue) {
        for (InputEnum value : InputEnum.values()) {
            if (inputValue.equals(value.getType())) {
                return value;
            }
        }
        return VEHICLE_INSURANCE_STATUS; //default
    }

}
