package com.vehicleinsuranceutils.dao;


import com.vehicleinsuranceutils.model.Vehicle;

import java.util.List;

public interface VehicleDao {

    Vehicle getVehicleByPlateNumber(String plateNumber);

    List<Vehicle> getAllVehicles();

    List<Vehicle> getForecomingExpiries(String dateRange);

}
