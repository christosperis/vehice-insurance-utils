package com.vehicleinsuranceutils.dao;

import com.vehicleinsuranceutils.model.Vehicle;

import java.util.List;

public interface OwnerDao {

    List<Vehicle> fetchOwnerVehicles(Integer ownerId);

}
